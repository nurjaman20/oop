<?php
require_once("animal.php");
require_once("frog.php");
require_once("ape.php");

$sheep = new Animal("shaun");
echo "Nama: $sheep->name<br>"; // "shaun"
echo "Hewan Berkaki $sheep->legs<br>"; // 2
echo "$sheep->cold_blooded<br><br>"; // false

$sungokong = new Ape("kera sakti");
echo "Nama: $sungokong->name<br>";
echo "Hewan Berkaki $sungokong->legs<br>";
echo "$sungokong->cold_blooded<br>";
echo "Bersuara ";$sungokong->yell(); // "Auooo"
echo "<br><br>";

$kodok = new Frog("buduk");
echo "Nama: $kodok->name<br>";
echo "Hewan Berkaki $kodok->legs<br>";
echo "$kodok->cold_blooded<br>";
echo "Berjalan Melompat ";$kodok->jump() ; // "hop hop"
?>